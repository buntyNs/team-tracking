import React from "react";
import ReactDOM from "react-dom";
import Map from "./component/map";




const places = [
  // {
  //   profileId: 1,
  //   longitude: 147.204751,
  //   latitude: -33.0029954,
    
  // },
  // {
  //   profileId: 2,
  //   longitude: 150.6517938,
  //   latitude: -33.847927,
    
    
  // },
  // {
  //   profileId: 3,
  //   longitude: 144.7729561,
  //   latitude: -37.9722342,
    
    
  // },
 
];



class App extends React.Component {


  render(){
  return (
    <div className="App">
      <Map
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDUC-vnqkz0_0VMqXjlOA2yaJUFToCJk&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100vh` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        // center={{ lat: 8.94090, lng: 76.791244 }}
        zoom={4}
        places={places}
      />
    </div>
  );
  }
}

export default App;
