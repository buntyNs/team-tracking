import React from "react";
import {
  withGoogleMap,
  GoogleMap,
  Marker,
  withScriptjs,
  InfoWindow
} from "react-google-maps";
import axios from 'axios';
import Sidebar from '../sidebar';
import RightSidebar from '../righsidebar';

const Markers = ({ places, markerClick }) => {
  return places.map(place => {
    return (
      <Marker key={place.profileId} position={{ lat: parseFloat(place.latitude), lng: parseFloat(place.longitude) }} onClick={() => { markerClick(place) }}>
        {/* <InfoWindow>
          <h4>{place.profileId}</h4>
        </InfoWindow> */}
      </Marker>
    );
  });
};

const Map = ({ places, zoom, center, markerClick, onMapMounted }) => {
  const exampleMapStyles = [
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
        {
          color: "#eeeeee",
        },
      ],
    },
    {
      featureType: "poi",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "water",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#9e9e9e",
        },
      ],
    },
  ];


  return (
    <GoogleMap
      zoom={zoom}
      center={center}
      ref={onMapMounted}
      options={{
        styles: exampleMapStyles,
      }}

    >
      <Markers places={places} markerClick={markerClick} />
    </GoogleMap>
  );
};


class MapWithMarker extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      places: this.props.places,
      center: { lat: 8.94090, lng: 76.791244 },
      zoom: 4,
      url: "http://localhost:8080/api/v1/team",
      marker: "",
      tempMarker :{},
      sidebarOpen: false,
      tempTeam: [],
      path:[],

    };

    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);//initialize initial state from props
    this.showAllTeam = this.showAllTeam.bind(this);
  }

  

  onMarkerClick = (marker) => {
    this._map.panTo({ lat: marker.latitude, lng: marker.longitude });
    this.setState(
      {
        marker: marker.profileId,
        tempMarker:marker,
        places: [marker],
        zoom: 15
      }
    )
    this.onSetSidebarOpen(true)
  }

  addPlace() {
    if (this.state.marker == "") {
      axios.get('http://localhost:8080/api/v1/team')
        .then((response) => {
          this.setState(prevState => ({
            places: response.data,
            tempTeam: response.data

          }));


        })
        .catch((error) => {
          console.log(error);
        })
    } else {
      const pmsId = [
        parseInt(this.state.marker)
      ];
      axios.post(`http://localhost:8080/api/v1/team/team_location`, pmsId)
        .then(response => {
          this.setState(prevState => ({
            places: response.data,
            tempMarker:response.data
          }));
        })
    }


  }

  componentDidMount() {
    axios.get('http://localhost:8080/api/v1/team')
      .then((response) => {
        this.setState(prevState => ({
          places: response.data,
          tempTeam: response.data

        }));
      })
      .catch((error) => {
        console.log(error);
      })
    this.intervalId = setInterval(this.addPlace.bind(this), 5000);
  }
  componentWillUnmount() {
    clearInterval(this.intervalId);
  }



  handleMapMounted = (map) => {
    this._map = map;
  }

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }

  showAllTeam = () => {
    console.log("test");
    this.setState({
      places: this.state.tempTeam,
      marker: "",
      zoom: 4,
      sidebarOpen: false
    });
  }


  render() {
    return (
      <div>
        <Sidebar sidebarOpen={this.state.sidebarOpen} onSetSidebarOpen={this.onSetSidebarOpen} />
        <Map
          center={this.state.center}
          zoom={this.state.zoom}
          places={this.state.places}
          markerClick={this.onMarkerClick}
          onMapMounted={this.handleMapMounted}
        />
        <RightSidebar showAllTeam={this.showAllTeam} />
      </div>


    );
  }
}

export default withScriptjs(withGoogleMap(MapWithMarker));
