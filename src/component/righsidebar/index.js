import React from "react";
import './rightsidebar.css';
import { FaWindowClose , FaMapMarkerAlt} from 'react-icons/fa';
const RightSidebar = ({ showAllTeam }) => {

    return (
        <div className="icon-bar">
            <div className="tooltip" onClick={()=>showAllTeam()}><FaMapMarkerAlt/><span className="tooltiptext">All Teams</span></div>
            <div  className="tooltip"><FaWindowClose/><span className="tooltiptext">Tooltip text</span></div>
            <div  className="tooltip"><FaWindowClose/><span className="tooltiptext">Tooltip text</span></div>
            <div  className="tooltip"><FaWindowClose/><span className="tooltiptext">Tooltip text</span></div>
            <div  className="tooltip"><FaWindowClose/><span className="tooltiptext">Tooltip text</span></div>
        </div>

    )
};

export default RightSidebar