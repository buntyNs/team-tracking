
import React from "react";
import './sidebar.css';
import { FaWindowClose } from 'react-icons/fa';
const Sidebar = ({sidebarOpen,onSetSidebarOpen}) => {
  
    return (
        <div className = {sidebarOpen?"open_left":"close_left"}>
          <FaWindowClose style = {{float:"right",margin:"10px",color:"white"}} onClick = {()=>onSetSidebarOpen(false)}/>
          </div>
         
    )
  };

  export default Sidebar